
<?php 
/** 
 * The main template file 
 * 
 * This is the most generic template file in a WordPress theme 
 * and one of the two required files for a theme (the other being style.css). 
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists. 
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/ 
 * 
 * @package SitesDoneQuick 
 */ 
 
get_header(); ?> 
  <div id="primary" class="content-area"> 
  <h2>Featured projects</h2>
    <main id="main" class="site-main">
 
       <?php  
                    // WP_Query arguments 
                $args = array( 
                    'post_type'              => array( 'portfolio' ), 
                    'posts_per_page'         => '3', 
                    'category_name'          => 'Portfolio Featured' 
                ); 
 
                // The Query 
                $portfolio = new WP_Query( $args ); 
 
                // The Loop 
                if ( $portfolio->have_posts() ) { 
                    
                    $i = 1; 
                    while ( $portfolio->have_posts() ) { 
                        $portfolio->the_post(); 
                        echo '<div class="row">';
                        ?> 
                            <?php  
                                if ($i % 2 == 1) { ?>

                                    <div class="col-12 col-md-6">
                                        <?php the_post_thumbnail('portfolio_grid'); ?>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
                                        <?php the_excerpt(); ?>
                                        <button type="button"><a class="btn" href=<?php the_permalink(); ?>>VIEW PROJECT DETAILS  <i class="fa fa-arrow-right" aria-hidden="true"></i></a></button> 
                                    </div>
                                    
                                    <?php
                                } else { ?>

                                <div class="col-12 col-md-6 order-2 order-md-1">
                                   <?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
                                   <?php the_excerpt(); ?>
                                   <button type="button"><a class="btn" href=<?php the_permalink(); ?>>VIEW PROJECT DETAILS  <i class="fa fa-arrow-right" aria-hidden="true"></i></a> </button>
                               </div>
                                   <div class="col-12 col-md-6 order-1 order-md-2">
                                   <?php the_post_thumbnail('portfolio_grid'); ?>
                               </div>
                               
                                    <?php
                                } 
                            ?> 
                        <?php 
                     $i++; 
                     echo '</div>';
                    }

                } else { 
                    // no posts found 
                    get_template_part( 'template-parts/content', 'none' ); 
 
                } 
 
                // Restore original Post Data 
                wp_reset_postdata(); 
        
       ?>  
 
 
 
 
 
  <h2>Other projects</h2>
    <?php 
    if ( have_posts() ) : 
      ?> 
      <div class="row"> 
      <?php 
      if ( is_home() && ! is_front_page() ) : ?> 
        <header> 
          <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1> 
        </header> 
 
      <?php 
      endif; 
 
      /* Start the Loop */ 
      while ( have_posts() ) : the_post(); 
 
        /* 
         * Include the Post-Format-specific template for the content. 
         * If you want to override this in a child theme, then include a file 
         * called content-___.php (where ___ is the Post Format name) and that will be used instead. 
         */ 
        get_template_part( 'template-parts/content', 'projects' ); 
 
      endwhile; 
      ?> 
      <div class="col-12"> 
      <?php 
      the_posts_pagination(array ( 
        'prev_text'    => '<', 
        'next_text'    => '>', 
      )); 
      ?> 
      </div> 
      <?php 
 
    else : 
 
      get_template_part( 'template-parts/content', 'none' ); 
 
    endif; ?> 
    </div><!-- .row --> 
    </main><!-- #main --> 
  </div><!-- #primary --> 
 
<?php 
get_sidebar(); 
get_footer(); 
