<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SitesDoneQuick
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<div id="page" class="site">
	<div class="header-img">
   
<nav class="navbar navbar-shrink main-navigation" id="site-navigation">
        <div class="container-fluid">
            
            <div class="navbar-header page-scroll">
                <a class="navbar-brand page-scroll" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/img/logo.svg'; ?>"></a>
                <div id="nav-icon1" class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                     <span></span>
                     <span></span>
                     <span></span>
                     <span></span>
                </div>
			</div>
			
            <?php
            wp_nav_menu( array(
                'theme_location' => 'menu-1',
                'menu_id'        => 'primary-menu',
            ) );
             ?>
        </div>
        <!-- /.container-fluid -->
</nav>

    <!-- Header -->
    <header class="heading">
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in "><?php 
                if(is_home()) {
                    echo get_the_title(get_option( 'page_for_posts' ));
                }
                else {
                    echo get_the_title();
                }
                
                
                ?></div>
            </div>
        </div>
	</header>
</div>


 <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'sites-done-quick' ); ?></a>
	
	<div id="content" class="site-content">
