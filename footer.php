<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SitesDoneQuick
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
	
		</nav><!--social menu-->
		<div class="site-info">

			Copyright 2017 &reg; all rights reserved  <i class="fa fa-circle" style="font-size:10px"></i><a href="#"> Contact <i class="fa fa-circle" style="font-size:10px"></i></i> <a href="#"> About</a>
			<?php
				/* translators: 1: Theme name, 2: Theme author. */
				/*printf( esc_html__( 'Theme: %1$s by %2$s.', 'sites-done-quick' ), 'sites-done-quick', '<a href="https://protechig.com">ProTech Internet Group</a>' );*/

			?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>