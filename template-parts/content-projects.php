<?php 
/** 
 * Template part for displaying projects
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/ 
 * 
 * @package SitesDoneQuick 
 */ 
 
?> 
 
<article id="post-<?php the_ID(); ?>" class="col-12 col-md-4"> 
  <div class="post__content"> 
 
    <?php 
  if ( has_post_thumbnail() ) { ?> 
  <figure class="featured-image index-image"> 
    <a href="<?php echo esc_url( get_permalink() ) ?>" rel="bookmark"> 
      <?php 
      the_post_thumbnail('blog_grid'); 
      ?> 
    </a> 
    </figure><!-- .featured-image full-bleed --> 
  <?php } ?> 
    <header class="entry-header">  
      <?php 
      the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); 
       ?> 
    </header><!-- .entry-header --> 
     
    <div class="entry-content"> 
    
            <!--$length_setting = get_theme_mod('length_setting');--> 
  
      <button type="button" class="otherproject" ><a href=<?php the_permalink(); ?>>VIEW PROJECT DETAILS  <i class="fa fa-arrow-right" aria-hidden="true"></i></a></button> 
 
       
    </div><!-- .entry-content --> 
     
    <div class="continue-reading"> 
      <?php 
      $read_more_link = sprintf( 
        /* translators: %s: Name of current post. */ 
        wp_kses( __( 'Continue reading %s', 'sites_done_quick' ), array( 'span' => array( 'class' => array() ) ) ), 
        the_title( '<span class="screen-reader-text">"', '"</span>', false ) 
      ); 
      ?> 
    </div><!-- .continue-reading --> 
     
  </div><!-- .post__content --> 
</article><!-- #post-## -->