<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SitesDoneQuick
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php
            //the_content();
			?>
<section class="work">
			<div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-6 services-container text-center">
            <img src="<?php the_field('process_section_on_image'); ?>">
            <h4><?php the_field('step_one'); ?></h4>
                <h2><?php the_field('process_section_tittle'); ?></h2>
                <p class="paragraph"><?php the_field('step_description_one'); ?></p>
            </div>

            <div class="col-md-4 col-sm-6 services-container text-center">
            <img src="<?php the_field('process_section_on_image_one'); ?>">
                 <h4><?php the_field('step_two'); ?></h4>
                <h2><?php the_field('process_section_tittle_one'); ?></h2>
                <p class="paragraph"><?php the_field('step_description_two'); ?></p>
            </div>

            <div class="col-md-4 col-sm-6 services-container text-center">
            <img src="<?php the_field('process_section_on_image_two'); ?>">
            <h4><?php the_field('step_three'); ?></h4>
                <h2><?php the_field('process_section_tittle_two'); ?></h2>
                <p class="paragraph"><?php the_field('step_description_three'); ?></p>
                

            </div>
        </div>
    </div>
</section>
	<!--the work -->
	

<section class="goals">
	<div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-4 col-sm-6 services-container">
                <div class="row">
                    <div class="col-12 col-md-3 col-sm-12 align-self-center">
                        <img src="<?php the_field('our_main_work');?>"> 
                    </div>
                    <div class="col-12 col-md-9 col-sm-12">
                        <h3><?php the_field('our_main_work_tittle');?></h3>
                        <p><?php the_field('our_main_work_paragraph');?></p>
                    </div>
                </div> 
            </div>

            <div class="col-12 col-md-4 col-sm-6 services-container">
                <div class="row">
                    <div class="col-12 col-md-3 col-sm-12 align-self-center">
                    <img src="<?php the_field('our_main_work_one');?>">
                    </div>
                    <div class="col-12 col-md-9 col-sm-12">
                    <h3><?php the_field('our_main_work_tittle_one');?></h3>
                    <p><?php the_field('our_main_work_paragraph_one');?></p>
                    </div>
                </div> 
            </div>

            <div class="col-12 col-md-4 col-sm-6 services-container">
                <div class="row">
                    <div class="col-12 col-md-3 col-sm-12 align-self-center">
                    <img src="<?php the_field('our_main_work_two');?>">
                    </div>
                    <div class="col-12 col-md-9 col-sm-12">
                    <h3><?php the_field('our_main_work_tittle_two');?></h3>
                    <p><?php the_field('our_main_work_paragraph_two');?></p>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</section>
	<!--the goal -->


 <section class="carousel">
    <?php
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'resources' ),
    );

    // The Query
    $query = new WP_Query( $args );
    ?>

    <?php if ( $query->have_posts() ) : ?>
        <div class="row owl-carousel owl-theme">
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <div class="customer">
                <div class="excerpt">
                   <p class="text"> <?php the_excerpt(); ?></p>
                <div class="img-responsive "><?php the_post_thumbnail(); ?></div>
                <p><?php the_title(); ?></p>
                <a class="read-more" href="<?php the_permalink(); ?>">Read More...</a>
</div>
            </div>
        <?php endwhile; ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</section>


            <?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'sites-done-quick' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->


</article><!-- #post-<?php the_ID(); ?> -->
