<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SitesDoneQuick
 */

?>
<div class="container">
<?php
	if( have_rows('flexible_section2')):
		while(have_rows('flexible_section2')): the_row();
			get_template_part('template-parts/content-blocks/block-' . get_row_layout() );
		endwhile;
	endif;
?>
</div>