<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SitesDoneQuick
 */

?>

<article id="post-<?php the_ID(); ?>" class="row">
	<div class="col-12 col-md-8 offset-md-2">
	<header class="entry-header">

    <?php
	if ( has_post_thumbnail() ) { ?>
	<figure class="featured-image full-bleed">
		<?php
		the_post_thumbnail('sites-done-quick-full-bleed');
		?>
	</figure><!-- .featured-image full-bleed -->
    <?php } ?>
    
	<?php sites_done_quick_the_category_list(); ?>

    </header><!-- .entry-header -->
    
	<section class="post-content">
		
		<?php
		if ( !is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div class="post-content__wrap">
			<div class="post-content__body">
		<?php
		endif; ?>
		
		<div class="entry-content">
			<?php
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'sites_done_quick' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );

				
			?>
		</div><!-- .entry-content -->
		
	</section><!-- .post-content -->
	</div>
	
	
</article><!-- #post-## -->
<div class="container-fluid">
<?php
	if( have_rows('flexible_sections')):
		while(have_rows('flexible_sections')): the_row();
			get_template_part('template-parts/content-blocks/block-' . get_row_layout() );
		endwhile;
	endif;
?>
</div>
