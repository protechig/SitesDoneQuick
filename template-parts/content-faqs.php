<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SitesDoneQuick
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div>
     <?php

        // check if the repeater field has rows of data
        if( have_rows('faqs') ):

            // loop through the rows of data
            while ( have_rows('faqs') ) : the_row();
            ?>
            <div class="faqs-info">      
            <div class="question">
            <i class="fas fa-caret-right"></i>
            <?php 
             // display a sub field value
                the_sub_field('question'); 
                ?>
            </div>

             <div class="answer">
              <?php the_sub_field('answer'); 
              ?>   
            </div>  
        </div> 
            <?php
            endwhile;

        else :

            // no rows found

        endif;

        ?>
</div>	
</article><!-- #post-## -->
