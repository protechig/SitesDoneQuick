<div class="row block-image_left d-flex flex-row" style="background-color: <?php the_sub_field('background_color'); ?>">
<div class="col-6 d-flex">
<img src="<?php the_sub_field('left_image'); ?>">
</div>

<div class="col-5 py-5 pl-5 d-flex flex-column justify-content-center">
   <h1> <?php the_sub_field('title'); ?></h1>
   <h2><?php the_sub_field('title'); ?></h2>
   <p> <?php  the_sub_field('content'); ?></p>

</div>

</div>