<div class="row block-image_right">

<div class="col-12 col-md-6 d-flex align-items-center justify-content-center">
   <img src="<?php the_sub_field('himage_left'); ?>">
</div>

<div class="col-12 col-md-6 ">
   <h1> <?php the_sub_field('htitle'); ?></h1>
   <h2><?php the_sub_field('hsub_title'); ?></h2>
   <p> <?php  the_sub_field('hcontent'); ?></p>
</div>
</div>