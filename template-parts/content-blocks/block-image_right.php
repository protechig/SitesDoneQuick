<div class="row block-image_right py-5" style="background-image: url(<?php the_sub_field('background_image');  ?>); background-size: cover;">
<div class="col-6 col-md-5 offset-md-1">
   <h3> <?php the_sub_field('title'); ?></h3>
   <p> <?php  the_sub_field('content'); ?></p>
</div>

<div class="col-6 d-flex align-items-center justify-content-center">
   <img src="<?php the_sub_field('image_right'); ?>">
</div>

</div>