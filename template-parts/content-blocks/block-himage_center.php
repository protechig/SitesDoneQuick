<div class="center align-items-center">
<img src="<?php the_sub_field('himage_center'); ?>">
<h1><?php the_sub_field('htitle_center'); ?></h1>
<h2><?php the_sub_field('hsub_title_center'); ?></h2>
<p><?php the_sub_field('hcontent_center'); ?></p>
</div>